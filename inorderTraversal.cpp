/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<int> inorderTraversal(TreeNode* root) {
        vector<int> values;
        
        if(root){
            if(root->left){
                for(auto v: inorderTraversal(root->left)){
                    values.push_back(v);        
                }
            }
            values.push_back(root->val);
            if(root->right){
                for(auto v: inorderTraversal(root->right)){
                    values.push_back(v);        
                }
            }
        }
        return values;
    }
};
