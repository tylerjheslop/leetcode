/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> levelOrder(TreeNode* root) {
        vector<vector<int>> values;
        // create queue, add root element 
        // begin loop based on queue size
        // add root to vector
        // pop front to TreeNode pointer
        // if left node, add to queue
        // if right node, add to same queue
        // push to vector of vectors
        // loop while queue > null
        
        TreeNode* current;
        std::queue<TreeNode*> q;
        q.push(root);
        if(root){
             while(!q.empty()){
                 vector<int> level;
                 int size = q.size();
                 
                 for(int i = 0; i < size; i++){
                     current = q.front();
                     level.push_back(current->val);
                     q.pop();

                     if(current->left){
                         q.push(current->left);
                     }
                     if(current->right){
                         q.push(current->right);
                     }    
                 }
                 
                 values.push_back(level);
            }
        }
        
        return values;
    }
};
